**Del diseño
falta enlazar las páginas es decir que al dar al botón de moodle los redirija al mockup de plataforma
esta opción se habilita en el botón como link


**

Falta específicar que datos se deben de proporcionar para poder hacer válido el pago con tarjeta citibanamex y como es que se va a reconocer que es de este tipo de tarjeta y no de otro banco 

Por ejemplo para las tarjetas los primeros 4 dígitos indican el banco y el tipo de tarjeta esto hay que específicar o hacer referencia a algún documento que contenga esta información para que al programador y tester les sea fácil plasmar estas condiciones en código y revisar su correcto funcionamiento.

Ahora hablando de funcionalidades hay que tener en claro que para pagos con tarjetas hay que subdividir las historias de usuarios a su forma más pequeña, para que sea más sencillo su procesamiento por lo cual debiesen considerar subdividir referente a la tarjeta de la siguiente forma:
     - Dar de alta tarjeta citibanamex (débito)
     - Dar de alta tarjeta citibanamex (crédito)
     - Realizar la verificación de tarjeta citibanamex (está deberá ser una historia con comunicación a los servicios bancarios responsables de está acción, pero dentro del sistema debemos marcarla porque tendremos que construir el código interno que hará la petición y obtendrá la respuesta y se deberá verificar el correcto funcionamiento)
     - Eliminar tarjeta citibanamex 
     - Actualizar datos de tarjeta citibanamex
     - Generar pago con tarjeta citibanamex (aquí el usuario solicita realizar el pago con este método). Proporcionar el código (CVC) de la tarjeta
     - Validar pago con tarjeta citibanamex (aquí el sistema debe hacer uso de los servicios bancarios para validar que la cuenta tiene saldo suficiente y que es correcta la información proporcionada)

Estas funcionalidades descritas representarán en consecuencias cada una, una historia de usuario diferente.

Cuidar en los criterios de aceptación que si se trata del llenado de un formulario debiesemos de especificar que datos va a capturar y si se cree conveniente inclusive especificar el tipo de dato y su longitud para que la estructura lógica de datos sea uniforme y congruente para todo el sistema.

Considerar en los criterios agregar hacia que página manda la acción una vez realizada o rechazada, para que sirva de validación del flujo de información dentro del sistema
